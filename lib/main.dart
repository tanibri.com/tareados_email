import 'package:tareaflutter_email/model/email.dart';
import 'package:flutter/material.dart';
import 'package:tareaflutter_email/model/backend.dart';
import 'package:tareaflutter_email/screens/detail_screen.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MiAplicacion",
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  Inicio({Key? key}) : super(key: key);

  @override
  State<Inicio> createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  List<Email> emails = Backend().getEmails();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 255, 0, 140),
          centerTitle: true,
          title: Text("Mock mail"),
        ),
        body: ListView.builder(
            itemCount: Backend().getEmails().length,
            itemBuilder: (context, index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              
              return Dismissible(
                key: ObjectKey(email),
                child:  ListTile(
                        
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            
                            Text(
                              email.from,
                              style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                            Text(
                        email.dateTime.toString(),
                        style: TextStyle(color: Colors.black, fontSize: 10),
                      ),
                          ],                         
                        ), 
                                              
                        subtitle: Container(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                 Icon(
                                    read
                                        ? Icons.brightness_1_outlined
                                        : Icons.brightness_1,
                                    color: Color.fromARGB(255, 255, 0, 140),
                                ),
                                Text(email.subject,
                                    style: TextStyle(
                                      fontSize: 15,
                                    )), 
                              ]),
                        ),
                        onLongPress: () {
                          Backend().markEmailAsRead(id);
                          setState(() {});
                        },
                        onTap: () {
                          Backend().markEmailAsRead(id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                    email: emails[index],
                                  )));
                          setState(() {});
                        },
                      ),
                
                onDismissed: (direction) {
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text( "$id")));
                    setState(() {});
                  });
                },
              );
            }
      )
    );
  }
}
