import 'package:flutter/material.dart';
import 'package:tareaflutter_email/model/email.dart';

class DetailScreen extends StatelessWidget {
  Email email;
  DetailScreen({Key? key, required this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject),
        backgroundColor: Color.fromARGB(255, 255, 0, 140),
        centerTitle: true,
      ),
      body: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(top: 15, left: 10),
            child: Text(
              "From: ${email.from}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
          ),
        ]),
        Divider(
          color: Color.fromARGB(255, 255, 0, 140),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
                padding: EdgeInsets.only(left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(email.subject),
                  ],
                )),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Text(email.dateTime.toString(),
                      style: TextStyle(fontSize: 10)),
                ),
              ],
            )
          ],
        ),
        Divider(
          color: Color.fromARGB(255, 255, 0, 140),
        ),
        Padding(
          padding: EdgeInsets.only(top: 3, left: 10, right: 10),
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: Text(
                  email.body,
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        )
      ]),
    );
  }
}
