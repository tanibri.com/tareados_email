import 'package:tareaflutter_email/model/email.dart';
import 'package:flutter/material.dart';

class ContentWidget extends StatelessWidget {
  
  final Email email;
  
  ContentWidget(this.email);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text('From',
                  textAlign: TextAlign.left,
                  style:
                      TextStyle(fontSize: 17.0, fontStyle: FontStyle.italic)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(email.from,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.black38)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(email.subject,
                  textAlign: TextAlign.left,
                  style:
                      TextStyle(fontSize: 17.0, fontStyle: FontStyle.italic)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(email.dateTime.toString(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.black38)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                email.body,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
